//
//  ViewController.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
  
  let provider = ImageProvider()
  let saver = ImageSaver()
  
  let disposeBag = DisposeBag()
  
  lazy var processingScheduler: OperationQueueScheduler = {
    let queue = OperationQueue()
    queue.maxConcurrentOperationCount = 10
    queue.qualityOfService = .userInitiated
    return OperationQueueScheduler(operationQueue: queue)
  }()
        
  override func viewDidLoad() {
    super.viewDidLoad()
    let images = (0..<1000).map { index in self.provider.getImage(index: index).subscribeOn(processingScheduler) }
        
    let start = Date()
    Observable
      .from(images)
      .merge(maxConcurrent: 10)
      .flatMap { image in
        return self.saver.saveImage(image).subscribeOn(self.processingScheduler)
      }
      .toArray()
      .subscribe(onSuccess: { res in
        self.printResults(res)
        print(Date().timeIntervalSince(start))
      }).disposed(by: disposeBag)
  }
    
  private func printResults(_ results: [Result<Void, Error>]) {
    let saveResult = results.reduce(into: (0, 0)) { (counter, result) in
      switch result {
        case .failure:
          counter.0 += 1
        case .success:
          counter.1 += 1
        }
      }
    
      print("Operation complete. Failed - \(saveResult.0); Saved - \(saveResult.1)")
  }
}
