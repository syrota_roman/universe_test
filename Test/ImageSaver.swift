//
//  ImageSaver.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import Foundation
import RxSwift

class ImageSaver {
  
  private let semaphore = DispatchSemaphore(value: 1)
  
  func saveImage(_ image: UIImage) -> Observable<Result<Void, Error>> {
    return autoreleasepool {
      return Observable<Result<Void, Error>>.create { observer in
        print("save")
        self.semaphore.wait()
        
        let id = UUID()
        let url = FileManager.default
          .urls(for: .documentDirectory, in: .userDomainMask)[0]
          .appendingPathComponent(id.uuidString)
          .appendingPathExtension("jpeg")
        if let imageData = image.jpegData(compressionQuality: 1) {
          do {
            try imageData.write(to: url)
            observer.onNext(.success(()))
            observer.onCompleted()
          } catch {
            observer.onNext(.failure(error))
            observer.onCompleted()
          }
        } else {
          observer.onNext(.failure(NSError(domain: "Image Saving", code: 100, userInfo: nil)))
          observer.onCompleted()
        }
        
        self.semaphore.signal()
          
        return Disposables.create()
      }
    }
  }
}
